# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Fabian Bornschein <fabiscafe@archlinux.org>
# Contributor: Static_Rocket

pkgbase=asusctl
pkgname=(
  asusctl
  rog-control-center
)
pkgver=6.1.10
pkgrel=1
pkgdesc="A control daemon, tools, and a collection of crates for interacting with ASUS ROG laptops"
arch=('x86_64')
url="https://asus-linux.org"
license=('MPL-2.0')
makedepends=(
  'cargo'
  'clang'
  'cmake'
  'fontconfig'
  'git'
  'hicolor-icon-theme'
  'libayatana-appindicator'
  'libinput'
  'libusb'
  'seatd'
  'systemd'
)
source=("git+https://gitlab.com/asus-linux/asusctl.git#tag=$pkgver")
sha256sums=('6e434db4ab4651170803eb77f8534a4f7f5b2fb98a04db2d1a59bcd4201199d0')

prepare() {
  cd "${pkgbase}"
  export RUSTUP_TOOLCHAIN=stable
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "${pkgbase}"
  export RUSTUP_TOOLCHAIN=stable
  ARGS+=" --frozen" make build
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_asusctl() {
  pkgdesc="${pkgdesc/tools/CLI tools}"
  depends=(
    'gcc-libs'
    'glibc'
    'hicolor-icon-theme'
    'libusb'
    'systemd'
    'systemd-libs'
  )
  optdepends=(
    'acpi_call: fan control'
    'asusctltray: tray profile switcher'
    'rog-control-center: app to control asusctl'
    'supergfxctl: hybrid GPU control'
  )
  conflicts=('gnome-shell-extension-asusctl-gnome')
  install="${pkgbase}.install"

  cd "${pkgbase}"
  make DESTDIR="${pkgdir}" install

  _pick rogcc "${pkgdir}/usr/bin/rog-control-center" \
     "${pkgdir}/usr/share/applications/rog-control-center.desktop" \
     "${pkgdir}/usr/share/icons/hicolor/512x512/apps/rog-control-center.png" \
     "${pkgdir}/usr/share/rog-gui"
}

package_rog-control-center() {
  pkgdesc="App to control asusctl"
  depends=(
    'asusctl'
    'fontconfig'
    'freetype2'
    'gcc-libs'
    'glibc'
    'hicolor-icon-theme'
    'libayatana-appindicator'
    'libinput'
    'libxkbcommon'
    'seatd'
    'systemd-libs'
  )

  cp -r rogcc/* "${pkgdir}"
}
